class Move {
  constructor(cellId, nextMoveXs) {
    this.cellId = cellId;
    this.xsMove = nextMoveXs;
  }
}

class LinkedList {
  constructor() {
    this.size = 0;
  }

  add(value) {
    let node = new Node(value);
    node.next = null;

    if (this.tail) {
      this.tail.next = node;
      node.prev = this.tail;
    }

    this.tail = node;

    this.size += 1;
  }
}

class Node {
  constructor(value) {
    this.value = value;
    this.prev = null;
    this.next = null;
  }
}

class AppState {
  constructor() {
    this.finished = false;
    this.movesHistory = new LinkedList();
    this.lastMovedNode = null;
    this.moveNodeForRedo = null;
  }

  reset() {
    this.finished = false;
    this.movesHistory = new LinkedList();
    this.lastMovedNode = null;
  }
}

const appState = new AppState();

const undo = document.querySelector('.undo-btn');
const redo = document.querySelector('.redo-btn');

undo.addEventListener('click', () => {
  if (!appState.lastMovedNode || appState.finished) {
    return;
  }

  unmakeMove(appState.lastMovedNode.value);
  appState.moveNodeForRedo = appState.lastMovedNode;
  appState.lastMovedNode = appState.lastMovedNode.prev;

  configureHistoryButtons();
});

redo.addEventListener('click', () => {
  if (!appState.moveNodeForRedo || appState.finished) {
    return;
  }

  makeMove(appState.moveNodeForRedo.value);
  appState.lastMovedNode = appState.moveNodeForRedo;
  appState.moveNodeForRedo = appState.moveNodeForRedo.next;

  configureHistoryButtons();
});


field.addEventListener('click', (e) => {
  const cell = e.target;
  if (!isFiredOnCell(e) || !isClean(cell) || appState.finished) {
    return;
  }

  const move = createMove(cell);

  makeMove(move);

  //clear history of moves and add lastMovedNode as last one in list
  if (appState.lastMovedNode) {
    appState.lastMovedNode.next = null;
    appState.movesHistory.tail = appState.lastMovedNode;
    appState.moveNodeForRedo = null;
  }

  appState.movesHistory.add(move);
  appState.lastMovedNode = appState.movesHistory.tail;

  configureHistoryButtons();

  const result = isFinished();
  if(!result.xsWon && !result.osWon && !result.allCellsAreUsed){
    return;
  }

  appState.finished = true;

  const wonTitle = document.querySelector('.won-title');
  const wonMessage = document.querySelector('.won-message');
  wonTitle.classList.remove('hidden');

  let message = result.xsWon ? 'Crosses won!' : result.osWon ? 'Toes won!' : 'It\'s a draw!';

  wonMessage.innerText = message;
  if(result.xsWon || result.osWon){
    result.winCells.forEach(cell => { cell.classList.add(result.direction, 'win') });
  }

});

const isFiredOnCell = (e) => {
  if (e.target.classList.contains('cell')) {
    return true
  }
  return false;
};

const createMove = (cell) => {
  if (!appState.lastMovedNode || !appState.lastMovedNode.value.xsMove) {
    return new Move(cell.id, true);
  }

  return new Move(cell.id, false);
};

const makeMove = (move) => {
  const cell = getCell(move.cellId);
  if (move.xsMove) {
    cell.classList.toggle('ch');
  } else {
    cell.classList.toggle('r');
  }
};

const unmakeMove = (move) => {
  const cell = getCell(move.cellId);
  cell.classList.remove('ch', 'r');
};

const configureHistoryButtons = () => {
  if (!appState.finished || appState.lastMovedNode) {
    undo.disabled = false;
  } else {
    undo.disabled = true;
  }

  if (!appState.finished || appState.moveNodeForRedo) {
    redo.disabled = false;
  } else {
    redo.disabled = true;
  }
};

const isClean = (cell) => {
  if (cell.classList.contains('ch') || cell.classList.contains('r')) {
    return false;
  }
  return true;
};

const getCell = (cellId) => {
  return document.querySelector(`#${cellId}`);
};

const isFinished = () => {
  const results = [
    {lines: horisontalLines, direction: 'horizontal'},
    {lines: verticalLines, direction: 'vertical'},
    {lines: rightDiagonal, direction: 'diagonal-right'},
    {lines: leftDiagonal, direction: 'diagonal-left'},
  ].map((linesContainer) => {
    console.log(linesContainer.direction);
    const result = calculateLineResult(linesContainer.lines);
    result.direction = linesContainer.direction;
    return result
  });

  console.log(results);

  const result = results.reduce((result, item)=> {
    if (result.xsWon || result.osWon || result.allCellsAreUsed){
      return result;
    }
    return item;
  });

  return result;
};

const calculateLineResult = (lineProducer) => {
  const allCells = document.querySelectorAll('.cell');
  const squareSide = Math.sqrt(allCells.length);
  const lines = lineProducer(squareSide);
  const result = lines.reduce((wonResult, indexes) => {
    if (wonResult.xsWon || wonResult.osWon) {
      return wonResult;
    }
    const cells = indexes.map( index => getCell(index));
    const xsWon = cells.every(cell => cell.classList.contains('ch'));
    const osWon = cells.every(cell => cell.classList.contains('r'));
    const winCells = (xsWon || osWon) ? cells : null;
    const result = {
      xsWon,
      osWon,
      winCells,
    };
    return result;
  }, {});
  result.allCellsAreUsed = Array.from(allCells).every(cell => cell.classList.contains('ch') || cell.classList.contains('r'));
  return result;
};

const horisontalLines =  (squareSide) => {
  const result = [];
  for (let line = 0; line < squareSide; line++) {
    let lineItems = [];
    for (let i = 0; i < squareSide; i++) {
      lineItems.push(`c-${i + line * squareSide}`);
    }
    result.push(lineItems);
  }
  return result;
};

const verticalLines =  (squareSide) => {
  const result = [];
  for (let line = 0; line < squareSide; line++) {
    let lineItems = [];
    for (let i = 0; i < squareSide; i++) {
      lineItems.push(`c-${line + i * squareSide}`);
    }
    result.push(lineItems);
  }
  return result;
};

const rightDiagonal =  (squareSide) => {
  const result = [];
  let id = 0;
  result.push(`c-${id}`);
  for (let i = 1; i < squareSide; i++) {
    id = (squareSide * i) + i;
    result.push(`c-${id}`);
  }
  return [result];
};

const leftDiagonal =  (squareSide) => {
  const result = [];
  let id = squareSide - 1;
  result.push(`c-${id}`);
  for (let i = 1; i < squareSide; i++) {
    id = id + squareSide - 1;
    result.push(`c-${id}`);
  }
  return [result];;
};

